#!/bin/bash

# Função para conectar ao servidor
connect_to_server() {
    HOST=$(zenity --forms --title="Conectar ao Servidor" --text="Preencha as informações" \
        --add-entry="Host:" \
        --add-entry="Porta:" \
        --add-entry="Usuário:" \
        --add-password="Senha:")
    
    IFS="|" read -r host port user password <<< "$HOST"
    
    sshpass -p "$password" ssh -p "$port" "$user"@"$host" true 2>/dev/null
    if [ $? -eq 0 ]; then
        echo "Conectado ao servidor."
        return 0
    else
        zenity --error --title "Erro" --text "Erro na conexão. Verifique suas credenciais e tente novamente."
        return 1
    fi
}

# Função para baixar arquivo
download_file() {
    REMOTE_PATH=$(zenity --entry --title "Baixar Arquivo" --text "Digite o caminho remoto:")
    LOCAL_PATH=$(zenity --file-selection --save --confirm-overwrite)
    
    scp -P "$port" "$user"@"$host":"$REMOTE_PATH" "$LOCAL_PATH"
    
    if [ $? -eq 0 ]; then
        zenity --info --title "Download Concluído" --text "Download concluído: $REMOTE_PATH -> $LOCAL_PATH"
    else
        zenity --error --title "Erro" --text "Erro durante o download. Verifique o caminho remoto e tente novamente."
    fi
}

# Função para carregar arquivo
upload_file() {
    LOCAL_PATH=$(zenity --file-selection --title "Carregar Arquivo" --filename="$HOME")
    REMOTE_PATH=$(zenity --entry --title "Carregar Arquivo" --text "Digite o caminho remoto:")
    
    scp -P "$port" "$LOCAL_PATH" "$user"@"$host":"$REMOTE_PATH"
    
    if [ $? -eq 0 ]; then
        zenity --info --title "Upload Concluído" --text "Upload concluído: $LOCAL_PATH -> $REMOTE_PATH"
    else
        zenity --error --title "Erro" --text "Erro durante o upload. Verifique o caminho remoto e tente novamente."
    fi
}

# Loop principal
while true; do
    ACTION=$(zenity --list --title="SCP App" --text="Escolha uma ação" --column="" \
        "Conectar ao Servidor" \
        "Baixar Arquivo" \
        "Carregar Arquivo" \
        "Sair")
    
    case "$ACTION" in
        "Conectar ao Servidor")
            connect_to_server
            ;;
        "Baixar Arquivo")
            if [ -z "$host" ]; then
                zenity --error --title "Erro" --text "Por favor, conecte-se ao servidor primeiro."
            else
                download_file
            fi
            ;;
        "Carregar Arquivo")
            if [ -z "$host" ]; then
                zenity --error --title "Erro" --text "Por favor, conecte-se ao servidor primeiro."
            else
                upload_file
            fi
            ;;
        "Sair")
            break
            ;;
    esac
done
